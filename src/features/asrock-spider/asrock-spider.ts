/*
 * Copyright (c) Mario Zimmermann, 2019.
 */

import * as cheerio from 'cheerio';
var cheerioTableparser = require('cheerio-tableparser');
import request from 'request';
import {Response} from "request";
import {persistenceStore} from "../../helper/persistence-store/PersistenceStore";
import {bot} from "../telegram/telegram";

export default class ASRockSpider {

    url: string = 'https://www.asrock.com/mb/AMD/X570%20Taichi/bios.html';

    public get = async () => {

        request(this.url, (error: any, response: Response, body: any) => {
            if(error) { console.log('Error: ' + error) }

            if(response.statusCode >= 200 && response.statusCode <= 300) {
                const $ = cheerio.load(body);
                const version = this.analyzeTable($)
            }

        })
    };

    private analyzeTable = async ($: CheerioStatic): Promise<any> => {
        //@ts-ignore
        cheerioTableparser($);
        //@ts-ignore
        const tbody = $('tbody').first().parsetable(false, false, true);
        const data = tbody[0];
        // wundervoll, ich weiß D:
        const mostRecentASRockVersion = tbody[0][0];

        if (data === null || undefined) {
            console.log('Could not find any data');
            return
        }
        const actualUpdateCount: number = data.length;

        if(this.hasUpdates(actualUpdateCount) && persistenceStore.getIsFirstStart() === false) {

            const message = `Neues ASRock Bios Update (${mostRecentASRockVersion}) verfügbar!\n\n@IkeaSofa @Soda42 @Kevin0707`;
            await bot.sendMessage(message);

            persistenceStore.setCurrentUpdateCount(actualUpdateCount);

        } else if (this.hasUpdates(actualUpdateCount) && persistenceStore.getIsFirstStart()) {
            await this.handleFirstStart(mostRecentASRockVersion, actualUpdateCount)
        } else {
            console.log('Nothing new.')
        }
        return tbody[0][0]
    };

    private handleFirstStart = async (mostRecentASRockVersion: any, actualUpdateCount: number) => {

            const message: string = `Scheinbar ist das der erste Durchlauf der Spinne. Da noch kein bestehender Update Count vorhanden ist, kann ich jetzt gerade nur die aktuellste Version (${mostRecentASRockVersion}) wiedergeben. Sofern Ihr diese schon installiert habt, könnt' ihr die Nachricht getrost ignorieren.\n\n@IkeaSofa @Soda42 @Kevin0707`;

            await bot.sendMessage(message);

            persistenceStore.setIsFirstStart(false);
            persistenceStore.setCurrentUpdateCount(actualUpdateCount);

    };

    /**
     * Checks if the actual update count is higher than the current update count stored on the server.
     * @param {number} actualUpdateCount
     */
    private hasUpdates = (actualUpdateCount: number): boolean => {
        return actualUpdateCount > persistenceStore.getCurrentUpdateCount()
    };


}
