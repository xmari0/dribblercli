/*
 * Copyright (c) Mario Zimmermann, 2019.
 */

import Telegraf, {ContextMessageUpdate} from "telegraf";

class Bot {

    bot: any;
    channelId: string = process.env.TELEGRAM_CHANNEL_ID!;

    constructor() {
        this.bot = new Telegraf(process.env.TELEGRAM_API_KEY!)
    }

    public sendMessage = async (message: string) => {
        await this.bot.sendMessage(this.channelId, message)
    };

    public listen = async () => {

        this.bot.on('message', async (ctx: ContextMessageUpdate) => {

            /**
             * currently force unwrapping, need to find a way to guard it
             */
            const messageObj = ctx.message!;
            const text = messageObj.text!;


            await this.listenToAllFlag(text, ctx);
        })

    };

    public listenToAllFlag = async (message: string, ctx: ContextMessageUpdate) => {


       /*     if(message.includes('@all')) {
                const formatted = t!.replace('@all', '');
                await this.bot.telegram.deleteMessage(ctx.message!.chat.id, ctx.message!.message_id);
                await ctx.reply(`@IkeaSofa @TiZZYY95 @Kevin0707 @rxmario @Soda42\n\n${formatted}\n\n~ ${message.from ? message.from.username : 'hu$o ohne account'}`);
            }});
       await this.bot.launch()*/
    }
}

export const bot = new Bot();
