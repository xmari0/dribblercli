/*
 * Copyright (c) Mario Zimmermann, 2019.
 */

import NodePersist from 'node-persist'

enum JSONAttributeKeys {
    UPDATE_COUNT = 'ASROCK_UPDATE_COUNT',
    FIRST_START = 'FIRST_START'
}

class PersistenceStore {

    constructor() {
        NodePersist.init()
            .then(res => {
                if(this.getIsFirstStart() === undefined || null) { this.setIsFirstStart(true)}
            });
    }

    public getIsFirstStart(): boolean {
        return NodePersist.getItemSync(JSONAttributeKeys.FIRST_START)
    }

    public setIsFirstStart(isFirstStart: boolean) {
        NodePersist.setItemSync(JSONAttributeKeys.FIRST_START, isFirstStart)
    }

    /**
     * Returns the current update count.
     */
    public getCurrentUpdateCount(): number {
       return NodePersist.getItemSync(JSONAttributeKeys.UPDATE_COUNT) || 0;
    }

    /**
     *
     * @param {number} newUpdateCount
     */
    public setCurrentUpdateCount(newUpdateCount: number) {
        NodePersist.setItemSync(JSONAttributeKeys.UPDATE_COUNT, newUpdateCount)
    }

}

export const persistenceStore = new PersistenceStore();
