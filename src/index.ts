#!/usr/bin/env node

import ASRockSpider from "./features/asrock-spider/asrock-spider";
import {bot} from "./features/telegram/telegram";

const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
const path = require('path');
const program = require('commander');

clear();
console.log(
    chalk.blue(
        figlet.textSync('dribblerCLI', { horizontalLayout: 'full' })
    )
);


program
    .version('0.0.1', '-v, --vers', 'output the current version')
    .description("a superior CLI")
    .option('spider', 'Starts the ASRock Bios Spider', new ASRockSpider().get)
    .option('listen', 'Starts the ASRock Bios Spider', bot.listen)
    .parse(process.argv);

if (!process.argv.slice(2).length) {
    program.outputHelp()
}

