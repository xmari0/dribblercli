declare class Bot {
    bot: any;
    channelId: string;
    constructor();
    sendMessage: (message: string) => Promise<void>;
    listen: () => Promise<void>;
    all: () => Promise<void>;
}
export declare const bot: Bot;
export {};
