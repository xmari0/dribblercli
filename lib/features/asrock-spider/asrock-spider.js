"use strict";
/*
 * Copyright (c) Mario Zimmermann, 2019.
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var cheerio = __importStar(require("cheerio"));
var cheerioTableparser = require('cheerio-tableparser');
var request_1 = __importDefault(require("request"));
var PersistenceStore_1 = require("../../helper/persistence-store/PersistenceStore");
var telegram_1 = require("../telegram/telegram");
var ASRockSpider = /** @class */ (function () {
    function ASRockSpider() {
        var _this = this;
        this.url = 'https://www.asrock.com/mb/AMD/X570%20Taichi/bios.html';
        this.get = function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                request_1.default(this.url, function (error, response, body) {
                    if (error) {
                        console.log('Error: ' + error);
                    }
                    if (response.statusCode >= 200 && response.statusCode <= 300) {
                        var $ = cheerio.load(body);
                        var version = _this.analyzeTable($);
                    }
                });
                return [2 /*return*/];
            });
        }); };
        this.analyzeTable = function ($) { return __awaiter(_this, void 0, void 0, function () {
            var tbody, data, mostRecentASRockVersion, actualUpdateCount, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //@ts-ignore
                        cheerioTableparser($);
                        tbody = $('tbody').first().parsetable(false, false, true);
                        data = tbody[0];
                        mostRecentASRockVersion = tbody[0][0];
                        if (data === null || undefined) {
                            console.log('Could not find any data');
                            return [2 /*return*/];
                        }
                        actualUpdateCount = data.length;
                        if (!(this.hasUpdates(actualUpdateCount) && PersistenceStore_1.persistenceStore.getIsFirstStart() === false)) return [3 /*break*/, 2];
                        message = "Neues ASRock Bios Update (" + mostRecentASRockVersion + ") verf\u00FCgbar!\n\n@IkeaSofa @Soda42 @Kevin0707";
                        return [4 /*yield*/, telegram_1.bot.sendMessage(message)];
                    case 1:
                        _a.sent();
                        PersistenceStore_1.persistenceStore.setCurrentUpdateCount(actualUpdateCount);
                        return [3 /*break*/, 5];
                    case 2:
                        if (!(this.hasUpdates(actualUpdateCount) && PersistenceStore_1.persistenceStore.getIsFirstStart())) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.handleFirstStart(mostRecentASRockVersion, actualUpdateCount)];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        console.log('Nothing new.');
                        _a.label = 5;
                    case 5: return [2 /*return*/, tbody[0][0]];
                }
            });
        }); };
        this.handleFirstStart = function (mostRecentASRockVersion, actualUpdateCount) { return __awaiter(_this, void 0, void 0, function () {
            var message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        message = "Scheinbar ist das der erste Durchlauf der Spinne. Da noch kein bestehender Update Count vorhanden ist, kann ich jetzt gerade nur die aktuellste Version (" + mostRecentASRockVersion + ") wiedergeben. Sofern Ihr diese schon installiert habt, k\u00F6nnt' ihr die Nachricht getrost ignorieren.\n\n@IkeaSofa @Soda42 @Kevin0707";
                        return [4 /*yield*/, telegram_1.bot.sendMessage(message)];
                    case 1:
                        _a.sent();
                        PersistenceStore_1.persistenceStore.setIsFirstStart(false);
                        PersistenceStore_1.persistenceStore.setCurrentUpdateCount(actualUpdateCount);
                        return [2 /*return*/];
                }
            });
        }); };
        /**
         * Checks if the actual update count is higher than the current update count stored on the server.
         * @param {number} actualUpdateCount
         */
        this.hasUpdates = function (actualUpdateCount) {
            return actualUpdateCount > PersistenceStore_1.persistenceStore.getCurrentUpdateCount();
        };
    }
    return ASRockSpider;
}());
exports.default = ASRockSpider;
