export default class ASRockSpider {
    url: string;
    get: () => Promise<void>;
    private analyzeTable;
    private handleFirstStart;
    /**
     * Checks if the actual update count is higher than the current update count stored on the server.
     * @param {number} actualUpdateCount
     */
    private hasUpdates;
}
