"use strict";
/*
 * Copyright (c) Mario Zimmermann, 2019.
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var node_persist_1 = __importDefault(require("node-persist"));
var JSONAttributeKeys;
(function (JSONAttributeKeys) {
    JSONAttributeKeys["UPDATE_COUNT"] = "ASROCK_UPDATE_COUNT";
    JSONAttributeKeys["FIRST_START"] = "FIRST_START";
})(JSONAttributeKeys || (JSONAttributeKeys = {}));
var PersistenceStore = /** @class */ (function () {
    function PersistenceStore() {
        var _this = this;
        node_persist_1.default.init()
            .then(function (res) {
            if (_this.getIsFirstStart() === undefined || null) {
                _this.setIsFirstStart(true);
            }
        });
    }
    PersistenceStore.prototype.getIsFirstStart = function () {
        return node_persist_1.default.getItemSync(JSONAttributeKeys.FIRST_START);
    };
    PersistenceStore.prototype.setIsFirstStart = function (isFirstStart) {
        node_persist_1.default.setItemSync(JSONAttributeKeys.FIRST_START, isFirstStart);
    };
    /**
     * Returns the current update count.
     */
    PersistenceStore.prototype.getCurrentUpdateCount = function () {
        return node_persist_1.default.getItemSync(JSONAttributeKeys.UPDATE_COUNT) || 0;
    };
    /**
     *
     * @param {number} newUpdateCount
     */
    PersistenceStore.prototype.setCurrentUpdateCount = function (newUpdateCount) {
        node_persist_1.default.setItemSync(JSONAttributeKeys.UPDATE_COUNT, newUpdateCount);
    };
    return PersistenceStore;
}());
exports.persistenceStore = new PersistenceStore();
