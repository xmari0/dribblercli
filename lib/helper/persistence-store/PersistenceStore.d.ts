declare class PersistenceStore {
    constructor();
    getIsFirstStart(): boolean;
    setIsFirstStart(isFirstStart: boolean): void;
    /**
     * Returns the current update count.
     */
    getCurrentUpdateCount(): number;
    /**
     *
     * @param {number} newUpdateCount
     */
    setCurrentUpdateCount(newUpdateCount: number): void;
}
export declare const persistenceStore: PersistenceStore;
export {};
