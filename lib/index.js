#!/usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var asrock_spider_1 = __importDefault(require("./features/asrock-spider/asrock-spider"));
var telegram_1 = require("./features/telegram/telegram");
var chalk = require('chalk');
var clear = require('clear');
var figlet = require('figlet');
var path = require('path');
var program = require('commander');
clear();
console.log(chalk.blue(figlet.textSync('dribblerCLI', { horizontalLayout: 'full' })));
program
    .version('0.0.1', '-v, --vers', 'output the current version')
    .description("a superior CLI")
    .option('spider', 'Starts the ASRock Bios Spider', new asrock_spider_1.default().get)
    .option('listen', 'Starts the ASRock Bios Spider', telegram_1.bot.listen)
    .parse(process.argv);
if (!process.argv.slice(2).length) {
    program.outputHelp();
}
